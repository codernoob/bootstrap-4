$(document).ready(function () {
    $('#signinmodal').click(function () {
        $('#loginModal').modal('show');
    });
    $('#cancellogin').click(function () {
        $('#loginModal').modal('hide');
    });
    $('#closeloginmodal').click(function () {
        $('#loginModal').modal('hide');
    });

    $('#reservemodalbtn').click(function () {
        $('#reserveModal').modal('show');
    });
    $('#closelogin').click(function () {
        $('#reserveModal').modal('hide');
    });
    $('#cancelreserve').click(function () {
        $('#reserveModal').modal('hide');
    });
    $("#myCarousel").carousel({
        interval: 2000,
        pause: false
    });
    $("#carouselButton").click(function () {
        if ($("#carouselButton").children("span").hasClass('fa-pause')) {
            $("#mycarousel").carousel('pause');
            $("#carouselButton").children("span").removeClass('fa-pause');
            $("#carouselButton").children("span").addClass('fa-play');
        } else if ($("#carouselButton").children("span").hasClass('fa-play')) {
            $("#mycarousel").carousel('cycle');
            $("#carouselButton").children("span").removeClass('fa-play');
            $("#carouselButton").children("span").addClass('fa-pause');
        }
    });
});